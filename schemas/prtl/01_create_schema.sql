set echo on
set serveroutput on
spool /tmp/oradock/logs/prtl/01_create_schema.log

create user prtl identified by prtl;
grant connect to prtl;
grant create session to prtl;
grant alter session to prtl;
grant create table to prtl;
grant create view to prtl;
grant CREATE MATERIALIZED VIEW to prtl;
grant CREATE ANY MATERIALIZED VIEW to prtl;
grant grant any privilege to prtl;
grant unlimited tablespace to prtl;
grant resource to prtl;
grant SELECT ANY DICTIONARY to prtl with admin option;
grant select_catalog_role to prtl with admin option;
grant create database link to prtl;
grant create public database link to prtl;
grant execute on utl_mail to prtl;
grant execute on sys.dbms_lock to prtl;

create user prtl_user identified by prtl_user;
grant connect to prtl_user;
grant create session to prtl_user;
grant alter session to prtl_user;
grant resource to prtl;
grant SELECT ANY DICTIONARY to prtl_user with admin option;
grant select_catalog_role to prtl_user with admin option;
grant grant any privilege to prtl_user;
grant execute on utl_mail to prtl_user;
grant execute on sys.dbms_lock to prtl_user;
create user prtl_bislnk identified by prtl_bislnk;
grant connect to prtl_bislnk;
grant create session to prtl_bislnk;

create user bislnk identified by bislnk;
grant connect to bislnk;
grant create session to bislnk;

create user ngwmn identified by ngwmn;
grant connect to ngwmn;
grant create session to ngwmn;

begin
    dbms_java.grant_permission('prtl', 'SYS:java.io.FilePermission', '<<ALL FILES>>', 'read,write,delete,execute');
    dbms_java.grant_permission('prtl_user', 'SYS:java.io.FilePermission', '<<ALL FILES>>', 'read,write,delete,execute');
end;
/


exit