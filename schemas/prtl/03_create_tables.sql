set echo on
set serveroutput on
spool /tmp/oradock/logs/prtl/03_create_tables.log

create table USER_ACCOUNT_HS
(
    USER_ACCOUNT_HS_KEY NUMBER(10) not null
        constraint USAH_PK
            primary key,
    USER_ACCOUNT_KEY NUMBER(10) not null,
    CREATE_USER_NAME VARCHAR2(30) not null,
    CREATE_TS DATE not null,
    SSO_SECONDARY_ID VARCHAR2(30) not null,
    FAILED_LOGIN_ATTEMPTS_CNT NUMBER(10) default 0 not null,
    STATUS_TEXT VARCHAR2(20) default 'OK' not null,
    PIN_APPROVED_IND VARCHAR2(1) default '0' not null,
    PIN_VERIFIED_IND VARCHAR2(1) default '0' not null,
    PIN_LOCKED_IND VARCHAR2(1) default '0' not null,
    FULL_USER_NAME VARCHAR2(200) not null,
    CURRENT_EMAIL_ADDRESS_TEXT VARCHAR2(255) not null,
    LAST_LOGIN_TS DATE,
    SELF_REG_CONFIRMATION_NBR_TEXT VARCHAR2(100),
    SELF_REG_TEMP_PASSWORD_TEXT VARCHAR2(8),
    PIN_REQUEST_DATE DATE,
    PIN_TEXT VARCHAR2(64),
    PIN_APPROVED_DATE DATE,
    PIN_VERIFIED_DATE DATE,
    PIN_LOCKED_DATE DATE,
    MODIFY_USER_NAME VARCHAR2(30),
    MODIFY_TS DATE
)
/

create table USER_ACCOUNTS
(
    USER_ACCOUNT_KEY NUMBER(10) not null
        constraint UACC_PK
            primary key,
    CREATE_USER_NAME VARCHAR2(30) not null,
    CREATE_TS DATE not null,
    SSO_SECONDARY_ID VARCHAR2(30) not null,
    FAILED_LOGIN_ATTEMPTS_CNT NUMBER(10) default 0 not null,
    STATUS_TEXT VARCHAR2(20) default 'OK' not null,
    PIN_APPROVED_IND VARCHAR2(1) default '0' not null,
    PIN_VERIFIED_IND VARCHAR2(1) default '0' not null,
    PIN_LOCKED_IND VARCHAR2(1) default '0' not null,
    FULL_USER_NAME VARCHAR2(200) not null,
    CURRENT_EMAIL_ADDRESS_TEXT VARCHAR2(255) not null,
    LAST_LOGIN_TS DATE,
    SELF_REG_CONFIRMATION_NBR_TEXT VARCHAR2(100),
    SELF_REG_TEMP_PASSWORD_TEXT VARCHAR2(8),
    PIN_REQUEST_DATE DATE,
    PIN_TEXT VARCHAR2(64),
    PIN_APPROVED_DATE DATE,
    PIN_VERIFIED_DATE DATE,
    PIN_LOCKED_DATE DATE,
    MODIFY_USER_NAME VARCHAR2(30),
    MODIFY_TS DATE
)
/

create table SECURITY_QUESTION_CODES
(
    SECURITY_QUESTION_KEY NUMBER(10) not null
        constraint SECQ_PK
            primary key,
    CREATE_USER_NAME VARCHAR2(30) not null,
    CREATE_TS DATE not null,
    QUESTION_TEXT VARCHAR2(100) not null,
    BEGIN_DATE DATE not null,
    END_DATE DATE,
    MODIFY_USER_NAME VARCHAR2(30),
    MODIFY_TS DATE
)
/

create table USER_SECURITY_ANSWERS
(
    USER_SECURITY_ANSWER_KEY NUMBER(10) not null
        constraint USA_PK
            primary key,
    CREATE_USER_NAME VARCHAR2(30) not null,
    CREATE_TS DATE not null,
    UACC_USER_ACCOUNT_KEY NUMBER(10) not null
        constraint USA_UACC_FK
            references USER_ACCOUNTS,
    SECQ_SECURITY_QUESTION_KEY NUMBER(10) not null
        constraint USA_SECQ_FK
            references SECURITY_QUESTION_CODES,
    QUESTION_TYPE_TEXT VARCHAR2(10) default 'ACCOUNT' not null,
    SECURITY_ANSWER_TEXT VARCHAR2(200) not null,
    MODIFY_USER_NAME VARCHAR2(30),
    MODIFY_TS DATE
)
/

create table USER_EMAIL_CHANGE_AUDITS
(
    USER_EMAIL_CHANGE_AUDIT_KEY VARCHAR2(35) not null
        constraint EMCA_PK
            primary key,
    CREATE_USER_NAME VARCHAR2(30) not null,
    CREATE_TS DATE not null,
    UACC_USER_ACCOUNT_KEY NUMBER(10) not null
        constraint EMCA_UACC_FK
            references USER_ACCOUNTS,
    NEW_EMAIL_ADDRESS_TEXT VARCHAR2(255) not null,
    OLD_EMAIL_ADDRESS_TEXT VARCHAR2(255)
)
/

create table PORTLETS
(
    PORTLET_KEY NUMBER(10) not null
        constraint PORT_PK
            primary key,
    CREATE_USER_NAME VARCHAR2(30) not null,
    CREATE_TS DATE not null,
    TITLE_TEXT VARCHAR2(50) not null,
    URI_TEXT VARCHAR2(100) not null
        constraint PORT_UK
            unique,
    PORTLET_TYPE_TEXT VARCHAR2(15) not null,
    INTRO_TEXT VARCHAR2(255),
    TAGLINE_TEXT VARCHAR2(255) not null,
    CONTENT_TEXT VARCHAR2(255),
    DISPLAY_TYPE_TEXT VARCHAR2(10) not null,
    DISPLAY_ORDER_NBR NUMBER(2) not null,
    ICON_FILE_NAME VARCHAR2(30),
    PORT_PORTLET_KEY NUMBER(10)
        constraint PORT_PORT_FK
            references PORTLETS DEFERRABLE INITIALLY DEFERRED,
    MODIFY_USER_NAME VARCHAR2(30),
    MODIFY_TS DATE
)
/

create table DESTINATIONS
(
    DESTINATION_KEY NUMBER(10) not null
        constraint DEST_PK
            primary key,
    CREATE_USER_NAME VARCHAR2(30) not null,
    CREATE_TS DATE not null,
    PORT_PORTLET_KEY NUMBER(10) not null
        constraint DEST_PORT_FK
            references PORTLETS,
    TITLE_TEXT VARCHAR2(75) not null,
    URL_TEXT VARCHAR2(255),
    CONTENT_TEXT VARCHAR2(255) not null,
    DISPLAY_ORDER_NBR NUMBER(2) not null,
    METADATA_TEXT VARCHAR2(1000),
    MODIFY_USER_NAME VARCHAR2(30),
    MODIFY_TS DATE
)
/


create index USA_UACC_FK_I
    on USER_SECURITY_ANSWERS (UACC_USER_ACCOUNT_KEY)
/

create index USA_SECQ_FK_I
    on USER_SECURITY_ANSWERS (SECQ_SECURITY_QUESTION_KEY)
/

create index EMCA_UACC_FK_I
    on USER_EMAIL_CHANGE_AUDITS (UACC_USER_ACCOUNT_KEY)
/

create index PORT_CONTENT_TEXT_I
    on PORTLETS (CONTENT_TEXT)
/

create index PORT_TITLE_TEXT_I
    on PORTLETS (TITLE_TEXT)
/

create index PORT_PORT_FK_I
    on PORTLETS (PORT_PORTLET_KEY)
/

create index DEST_PORT_FK_I
    on DESTINATIONS (PORT_PORTLET_KEY)
/

create index DEST_METADATA_I
    on DESTINATIONS (METADATA_TEXT)
/

create index DEST_URL_TEXT_I
    on DESTINATIONS (URL_TEXT)
/

create index DEST_TITLE_TEXT_I
    on DESTINATIONS (TITLE_TEXT)
/

exit