set echo on
set serveroutput on
spool /tmp/oradock/logs/sys/01_acl.log

BEGIN
    DBMS_NETWORK_ACL_ADMIN.APPEND_HOST_ACE(
        host => '*',
        ace => xs$ace_type(privilege_list => xs$name_list('resolve'),
        principal_name => 'PUBLIC',
        principal_type => xs_acl.ptype_db)
    );
    DBMS_NETWORK_ACL_ADMIN.APPEND_HOST_ACE(
        host => '*',
        ace => xs$ace_type(privilege_list => xs$name_list('connect'),
        principal_name => 'PUBLIC',
        principal_type => xs_acl.ptype_db)
    );
END;
/
COMMIT;

ALTER SYSTEM SET SMTP_OUT_SERVER = 'smtp:1025' SCOPE=both;

exit