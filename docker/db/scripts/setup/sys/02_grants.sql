set echo on
set serveroutput on
spool /tmp/oradock/logs/sys/02_grants.log

begin
    for grnt in (
        select 'grant select on sys.' || object_name || ' to public with grant option' query 
        from all_objects 
        where owner = 'SYS' 
            and object_type in ('TABLE', 'VIEW') 
            and object_name not like '%$%' 
            and regexp_like(object_name, '^[A-Z]')
        order by object_name asc
    ) loop
        execute immediate ' ' || grnt.query;
    end loop;
end;

exit