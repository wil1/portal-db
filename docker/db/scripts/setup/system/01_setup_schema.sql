set echo on
set serveroutput on
spool /tmp/oradock/logs/system/01_setup_schema.log

create table system.whereami 
(
  oracle_platform varchar2(15) 
);

insert into system.whereami (oracle_platform) values ('ORADevelop12c');

commit;


create public synonym whereami for system.whereami;

grant select on whereami to public;

exit