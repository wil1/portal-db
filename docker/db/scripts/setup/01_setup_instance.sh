#!/bin/bash
ORACLE_SID="`grep $ORACLE_HOME /etc/oratab | cut -d: -f1`"
ORACLE_PDB="`ls -dl $ORACLE_BASE/oradata/$ORACLE_SID/*/ | grep -v pdbseed | awk '{print $9}' | cut -d/ -f6`"
ORAENV_ASK=NO
source oraenv


echo "Enabling extended data types and restarting database in upgrade mode"
sqlplus / as sysdba << EOF
      ALTER SYSTEM SET max_string_size=extended SCOPE=SPFILE;
      SHUTDOWN IMMEDIATE;
      STARTUP UPGRADE;
      ALTER PLUGGABLE DATABASE ALL OPEN UPGRADE;
      exit;
EOF

echo "Starting PDB conversion to extended data types"
mkdir /tmp/edt
cd /tmp/edt
$ORACLE_HOME/perl/bin/perl $ORACLE_HOME/rdbms/admin/catcon.pl -d $ORACLE_HOME/rdbms/admin -b utl32k_output $ORACLE_HOME/rdbms/admin/utl32k.sql
cat *.log
rm -rf /tmp/edt
echo "Completed PDB conversion to extended data types"

echo "Restarting database after enabling extended data types"
sqlplus / as sysdba << EOF
      SHUTDOWN IMMEDIATE;
      STARTUP;
      show parameter max_string_size;
      exit;
EOF
echo "Database restarted, extended data types enabled"
    
echo "Creating log output directories in /tmp/oradock/logs"
mkdir -p /tmp/oradock/logs/sys
mkdir -p /tmp/oradock/logs/system
mkdir -p /tmp/oradock/logs/prtl

# add aliases for ORADEV, DOPDEV, etc...
(cd /opt/oracle/scripts/setup/tweaks/ && sh /opt/oracle/scripts/setup/tweaks/tweakTnsnames.sh > /tmp/oradock/logs/tweaks/tweakTnsnames.log 2>&1)


echo "Installing UTL_MAIL"
sqlplus sys/$ORACLE_PWD@oradockpdb as sysdba @$ORACLE_HOME/rdbms/admin/utlmail.sql
sqlplus sys/$ORACLE_PWD@oradockpdb as sysdba @$ORACLE_HOME/rdbms/admin/prvtmail.plb

echo "Applying SYS schema changes"
sqlplus sys/$ORACLE_PWD@oradockpdb as sysdba @/opt/oracle/scripts/setup/sys/01_acl.sql

echo "Applying SYSTEM schema changes"
sqlplus system/$ORACLE_PWD@oradockpdb @/opt/oracle/scripts/setup/system/01_setup_schema.sql


echo "Creating PRTL schema"
sqlplus sys/$ORACLE_PWD@oradockpdb as sysdba @/tmp/schemas/prtl/01_create_schema.sql
sqlplus prtl/prtl@oradockpdb @/tmp/schemas/prtl/02_create_sequences.sql
sqlplus prtl/prtl@oradockpdb @/tmp/schemas/prtl/03_create_tables.sql
sqlplus prtl/prtl@oradockpdb @/tmp/schemas/prtl/04_create_triggers.sql
sqlplus prtl/prtl@oradockpdb @/tmp/schemas/prtl/05_insert_data.sql



echo "Recompiling invalid objects"
sqlplus sys/$ORACLE_PWD@oradockpdb as sysdba  <<  EOF
spool /tmp/oradock/logs/recompile.log
exec utl_recomp.recomp_serial;
EOF



echo "Listing invalid objects"
sqlplus sys/$ORACLE_PWD@oradockpdb as sysdba  <<  EOF
set serveroutput on
set linesize 32000
set pagesize 40000
spool /tmp/oradock/logs/invalid_objects.log
SELECT o.owner,
       o.object_type,
       o.object_name,
       o.status,
       e.type,
       e.line,
       e.position,
       e.text,
       e.attribute
FROM   dba_objects o
join   dba_errors e
    on o.owner = e.owner
    and o.object_name = e.name
WHERE  o.status = 'INVALID'
ORDER BY o.owner, o.object_type, o.object_name, e.sequence;
exit;
EOF


echo "Configuring C shell"
cat << EOF > /home/oracle/.cshrc
#!/bin/csh
#
#
#       settings for interactive shells
#
set cdpath=$HOME
set filec
set noclobber
set history=150
set savehistory = 150
setenv PATH ${PATH}:$HOME/bin
setenv ORACLE_HOME /opt/oracle/product/12.2.0.1/dbhome_1
setenv ORACLE_TERM vt100
setenv ORACLE_SID oradockpdb
setenv DBA /b03/sql_scripts/dba_tools
setenv ORACLE_BASE /opt/oracle
setenv TOOLS /b03/sql_scripts/dba_tools
setenv PATH ${PATH}:$HOME/bin:${ORACLE_HOME}:$ORACLE_HOME/bin:${ORACLE_BASE}:.
setenv PRINTER oa1tcp
setenv EDITOR vi
limit coredumpsize 0
limit datasize unlimited
limit stacksize unlimited
EOF
